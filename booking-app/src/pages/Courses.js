//import coursesData from '../data/courses';
import { useState, useEffect } from 'react';
import CourseCard from '../components/CourseCard';

export default function Courses() {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetch('http://localhost:4000/courses/all')
    .then((res) => res.json())
    .then((data) => {
      const courseArray = data.map((course) => {
        return <CourseCard courseProp={course} key={course.id} />;
      });
      setCourses(courseArray);
    });
  }, [courses]);

  return <>{courses}</>;
}
