import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState('');

  const { user, setUser } = useContext(UserContext);

  function authenticate(e) {
    e.preventDefault();

    fetch('http://localhost:4000/users/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: 'Login successful',
            icon: 'success',
            text: 'Welcome to Booking App of 211'
          });
        } else {
          Swal.fire({
            title: 'Authentication failed!',
            icon: 'error',
            text: 'Check your login credentials'
          })
        }
      });

    const retrieveUserDetails = (token) => {
      fetch('http://localhost:4000/users/details', {
        headers: { Authorization: `Bearer ${token}`}
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        });
    };

    // Set email of authed user in local storage
    //localStorage.setItem('email', email);

    //setUser({
      //email: localStorage.getItem('email'),
    //});

    //alert('You are now logged in.');
    setEmail('');
    setPassword('');
  }

  useEffect(() => {
    if (email !== '' && password !== '') setIsActive(true);
    else setIsActive(false);
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/courses/"></Navigate>
  ) : (
    <>
      <h1>Login</h1>
      <Form onSubmit={(e) => authenticate(e)}>
        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button variant="success" type="submit" id="submitBtn">
            Submit
          </Button>
        ) : (
          <Button variant="muted" type="submit" id="submitBtn" disabled>
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}

export default Login;

// vim: ft=javascriptreact
