import { useEffect, useState } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function CourseCard({courseProp}) {
  //const [count, setCount] = useState(0);
  //const [seats, setSeats] = useState(10);
  let { name, description, price, _id} = courseProp;

  //function enroll() {
    //if (count !== 10) {
      //setCount(count + 1);
      //setSeats(seats - 1);
    //} else {
    //}
  //}

  //useEffect(() => {
    //if (seats === 0) {
      //alert('No more seats available.')
    //}
  //}, [count, seats]);

  /*

    ACTIVITY:
    Create a state hook that will represent the number of available seats for each course.

    Default 10, decrement by 1 for each enroll.
    add a condition that will show an alert "No more seats are available" if  seats state is 0.

  */

  return (
    <Row className="mt-3 mb-3">
      <Col>
        <Card>
          <Card.Body>
            <Card.Title>{name}</Card.Title>

            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>

            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PHP {price}</Card.Text>

    {/*<Card.Text>Enrollees: {count}</Card.Text>
            <Card.Text>Available seats: {seats}</Card.Text> */}
            <Button as={Link} to={`/courses/${_id}`}>Details</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

// vim: ft=javascriptreact
